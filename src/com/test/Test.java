package com.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Test implements Callable<String>{

	@Override
	public String call() throws Exception {
		Thread.sleep(1000);
		return Thread.currentThread().getName();
	}
	
	
	public static void main(String... args) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<Future<String>> list = new ArrayList<Future<String>>();
        
        Callable<String> callable = new Test();
        for(int i=0; i < 100; i++){
        	Future<String> future = executor.submit(callable);
//        	list.add(future);
        	System.out.println(new Date()+ "::"+future.get());
        	
        }
        
        for(Future<String> fut : list){
            try {
            	System.out.println(new Date()+ "::"+fut.get());
            	
            }catch (Exception e) {
				// TODO: handle exception
			}
        }
        

        executor.shutdown();	
	}
}
