package com.callable;

import java.util.concurrent.Callable;
import com.bean.EmpolyeeBean;
import com.util.StringUtil;

public class CallableDemo implements Callable<EmpolyeeBean>{
	

	@Override
	public EmpolyeeBean call() throws Exception {
		System.out.println("thread name :"+Thread.currentThread().getName());
		Thread.sleep(1000);

		EmpolyeeBean bean = new EmpolyeeBean();
		bean.setId(StringUtil.next());
		bean.setFullName("ammy thailand");
		bean.setSalary(900000000L);
		return bean;
	}

}
