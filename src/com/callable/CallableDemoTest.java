package com.callable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.bean.EmpolyeeBean;

public class CallableDemoTest {
	public static void main(String[] args) throws Exception{
		ExecutorService executor = Executors.newFixedThreadPool(10);		
		List<Future<EmpolyeeBean>> empList = new ArrayList<Future<EmpolyeeBean>>();	
		try {	
			for(int i = 0; i < 50; i++) {
				Callable<EmpolyeeBean> callable = new CallableDemo();
				Future<EmpolyeeBean> future = executor.submit(callable);
				empList.add(future);
			}
			
			for(int i = 0; i < empList.size(); i++) {
				LocalDateTime timeNow = java.time.LocalDateTime.now();
				System.out.println(timeNow+"| id :"+empList.get(i).get().getId());
				System.out.println(timeNow+"| fullName :"+empList.get(i).get().getFullName());
				System.out.println(timeNow+"| salary :"+empList.get(i).get().getSalary());	
			}
		
		}catch(Exception ex){
			System.out.println("ERROR :"+ex.getMessage());
		}finally {
			executor.shutdown();	
		}	
	}

}
