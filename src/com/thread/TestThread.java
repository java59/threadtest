package com.thread;

public class TestThread  extends Thread {
	
	private String threadName;
	
	TestThread(String threadName) {
		this.threadName = threadName;	
	}
	
	@Override
	public void run() {
			System.out.println("ThreadName :"+threadName);
	}
	
	public static void main(String... args) throws Exception{	
			for(int i = 1; i<=10; i++) {
				Thread t = new Thread(new TestThread("Thread number :"+i));
				t.start();
			}	
	}
}
