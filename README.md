Thread
1. thread เป็น Class บรรจุ method ต่างๆที่ทำงานเกี่ยวกับ thread
2. ทำ class ให้เป็น thread ด้วยการ extends
3. class ที่เป็น thread จะ run ได้ด้วยการเรียกใช้ method start() และไปทำงานที่ method run()
4. thread

Runnable
1. runnable เป็น interface บรรจุแค่ abstract method run() 
2. ทำ class ให้เป็น thread ด้วยการ implements
3. runnable ไม่สามารถเรียกใช้ thread ได้โดยตรงต้องทำการ new instance thread
4. class ที่เป็น thread จะ run ได้ด้วยการเรียกใช้ method start() และไปทำงานที่ method run()

Callable เหมือนกับ runnable ทุกๆอย่างเพิ่มเติมมาในส่วนของการ return